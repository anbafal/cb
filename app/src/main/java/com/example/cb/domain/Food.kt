package com.example.cb.domain

data class Food(val id: String, val name: String, val expired: String, val picture: String = "")