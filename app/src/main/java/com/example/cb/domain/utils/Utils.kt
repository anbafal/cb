package com.example.cb.domain.utils

import java.text.SimpleDateFormat
import java.util.*

object Utils {
    // todo: A way to avoid Singleton (to facilitate testing) unless have not a choice
    fun longDateToString(date: Long): String =
        SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).apply {
            timeZone = TimeZone.getTimeZone("UTC")
        }.format(date)

    //todo: Factorize with first function
    fun stringDateToLong(date: String): Long {
        val formatDate = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).apply {
            timeZone = TimeZone.getTimeZone("UTC")
        }.parse(date)
        return formatDate?.time ?: 0
    }

}

