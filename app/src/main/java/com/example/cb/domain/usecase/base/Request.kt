package com.example.cb.domain.usecase.base

import kotlinx.coroutines.CancellationException

class Request<T> {
    private var onComplete: ((T) -> Unit)? = null
    private var onError: ((String) -> Unit)? = null
    private var onCancel: ((CancellationException) -> Unit)? = null


    fun onComplete(block: (T) -> Unit) {
        onComplete = block
    }

    fun onError(block: (String) -> Unit) {
        onError = block

    }

    fun onCancel(block: (CancellationException) -> Unit) {
        onCancel = block
    }

    operator fun invoke(result: T) {
        onComplete?.invoke(result)
    }

    operator fun invoke(error: String) {
        onError?.invoke(error)
    }

    operator fun invoke(error: CancellationException) {
        onCancel?.invoke(error)
    }
}