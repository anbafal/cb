package com.example.cb.domain.usecase.base


import com.example.cb.domain.utils.DispatcherProvider
import kotlinx.coroutines.*
import javax.inject.Inject

typealias CompletionBlock<T> = Request<T>.() -> Unit

abstract class UseCase<T, P> {

    private var parentJob: Job = Job()

    @Inject
    lateinit var backgroundContext: DispatcherProvider

    @Inject
    lateinit var foregroundContext: DispatcherProvider

    protected abstract suspend fun executeOnBackground(params: P): T

    fun execute(params: P, block: CompletionBlock<T>) {
        val response = Request<T>().apply { block() }
        unsubscribe()
        parentJob = Job()
        CoroutineScope(foregroundContext.main() + parentJob).launch {
            try {
                val result = withContext(backgroundContext.io()) {
                    executeOnBackground(params)
                }
                response(result)
            } catch (cancellationException: CancellationException) {
                response(cancellationException)
            } catch (e: Exception) {
                //todo: Manage properly error
                response(e.message.toString())
            }
        }
    }


    fun unsubscribe() {
        parentJob.apply {
            cancelChildren()
            cancel()
        }
    }
}



