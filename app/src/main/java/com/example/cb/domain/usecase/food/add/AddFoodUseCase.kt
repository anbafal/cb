package com.example.cb.domain.usecase.food.add

import com.example.cb.domain.Food
import com.example.cb.domain.repository.IFoodRepository
import com.example.cb.domain.usecase.base.UseCase
import com.example.cb.framework.toBean
import javax.inject.Inject

class AddFoodUseCase @Inject constructor(private val foodRepository: IFoodRepository) :
    UseCase<Long, Food>() {
    override suspend fun executeOnBackground(params: Food): Long {
        return params.toBean().let {
            foodRepository.addFood(params)
        }
    }
}