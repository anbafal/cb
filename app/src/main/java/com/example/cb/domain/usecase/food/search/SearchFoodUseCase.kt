package com.example.cb.domain.usecase.food.search

import com.example.cb.domain.Food
import com.example.cb.domain.repository.IFoodRepository
import com.example.cb.domain.usecase.base.UseCase
import javax.inject.Inject

class SearchFoodUseCase @Inject constructor(private val foodRepository: IFoodRepository) :
    UseCase<Food, String>() {
    override suspend fun executeOnBackground(params: String): Food {
        return foodRepository.searchFoodWithId(params)
    }
}