package com.example.cb.domain.repository

import com.example.cb.domain.Food

interface IFoodRepository {
    suspend fun searchFoodWithId(foodId: String): Food
    suspend fun addFood(food: Food): Long
}