package com.example.cb

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import com.example.cb.databinding.SingleActivityBinding
import com.example.cb.presentation.features.loader.IFragmentLoaderListener
import com.example.cb.presentation.features.loader.LoaderUI
import com.example.cb.presentation.utils.AppBarListener
import com.example.cb.presentation.utils.visibleWhen
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SingleActivity : AppCompatActivity(), AppBarListener, IFragmentLoaderListener {

    private lateinit var appBarConfiguration: AppBarConfiguration

    private lateinit var navController: NavController

    private lateinit var binding: SingleActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = SingleActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)

        // todo: if app grows exclude dynamically fragment that we don't want that arrow for back is not displayed

        val appBarConfiguration =
            AppBarConfiguration(setOf(R.id.splashFragment, R.id.foodListFragment))
        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.main_nav_host_fragment) as NavHostFragment

        navController = navHostFragment.navController
        navController.apply {
            NavigationUI.setupActionBarWithNavController(
                this@SingleActivity,
                this,
                appBarConfiguration
            )
        }
    }

    override fun onSupportNavigateUp(): Boolean = navController.navigateUp()

    override fun showLoader(loaderUI: LoaderUI) {
        with(binding) {
            coordinatorLayout.alpha = loaderUI.alphaScreen
            contentMain.progressBar.visibility = loaderUI.visibilityProgressBar
        }
        if (loaderUI.shouldDisabledUI()) {
            window.addFlags(LoaderUI.FLAG_ACTIVITY_DISABLED)
        } else {
            window.clearFlags(LoaderUI.FLAG_ACTIVITY_DISABLED)
        }
    }


    override fun displayAppBar(shouldDisplay: Boolean) {
        binding.appbar.visibleWhen(shouldDisplay)
    }

}