package com.example.cb.framework

import com.example.cb.domain.Food
import com.example.cb.domain.utils.Utils
import com.example.cb.framework.room.bean.FoodBean


fun FoodBean.toDomain() =
    Food(id = id, name = name, expired = Utils.longDateToString(expired), picture = picture)

fun Food.toBean() =
    FoodBean(id = id, name = name, expired = Utils.stringDateToLong(expired), picture = picture)