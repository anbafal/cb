package com.example.cb.framework.retrofit

object RetrofitConstants {
    //todo: base link could be also be put in BuildConfig
    const val BASE_LINK = "https://world.openfoodfacts.org/api/v0/"

    const val SEARCH_FOOD_LINK = "product"
}