package com.example.cb.framework.room

object RoomConstants {
    const val DATABASE_NAME = "AppDatabase"
    const val TABLE_FOOD = "foods"
}