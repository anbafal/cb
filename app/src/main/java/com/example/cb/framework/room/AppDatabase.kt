package com.example.cb.framework.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.cb.framework.room.bean.FoodBean
import com.example.cb.framework.room.dao.FoodDao


//todo: exportSchema remove before release app

@Database(
    entities = [FoodBean::class],
    version = 1,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun foodDao(): FoodDao

}