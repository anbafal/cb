package com.example.cb.framework.room.bean

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.cb.framework.room.RoomConstants

@Entity(tableName = RoomConstants.TABLE_FOOD)
class FoodBean(
    @PrimaryKey
    val id: String,
    val name: String,
    val expired: Long,
    val picture: String
)