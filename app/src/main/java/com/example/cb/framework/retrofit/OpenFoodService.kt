package com.example.cb.framework.retrofit

import com.example.cb.data.entities.FoodDto
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Path


interface OpenFoodService {

    @GET(RetrofitConstants.SEARCH_FOOD_LINK + "/{FOOD_ID}")
    fun searchFoodAsync(@Path("FOOD_ID") foodId: String): Deferred<FoodDto>
}