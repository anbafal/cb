package com.example.cb.framework.room.dao

import androidx.room.Dao
import androidx.room.Query
import com.example.cb.framework.room.bean.FoodBean


@Dao
interface FoodDao : BaseDao<FoodBean> {

    @Query("SELECT * FROM foods")
    suspend fun allFoods(): FoodBean?
}