package com.example.cb.presentation.features.loader

import androidx.lifecycle.LiveData

interface IViewModelLoader {
    val showLoading: LiveData<LoaderUI>
}