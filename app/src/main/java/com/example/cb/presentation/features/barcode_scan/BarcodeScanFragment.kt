package com.example.cb.presentation.features.barcode_scan

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.SurfaceHolder
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.example.cb.databinding.FragmentBarcodeScanBinding
import com.example.cb.presentation.base.BaseFragment
import com.example.cb.presentation.utils.toast
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class BarcodeScanFragment : BaseFragment<FragmentBarcodeScanBinding, BarcodeScanViewModel>(),
    View.OnClickListener {
    @Inject
    lateinit var detector: DetectorProvider

    @Inject
    lateinit var cameraSource: CameraProvider

    @Inject
    lateinit var localBroadcastManager: LocalBroadcastManager

    private var receiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            when (intent.action) {
                DetectorProvider.INTENT_CUSTOM_SCAN -> {
                    intent.getStringExtra(DetectorProvider.INTENT_CUSTOM_SCAN_DATA)?.let {
                        getViewModel().onScanResult(it)
                    }
                }
            }
        }
    }

    private val vm: BarcodeScanViewModel by viewModels()

    private val REQUIRED_PERMISSIONS = arrayOf("android.permission.CAMERA")

    private val REQUEST_CODE_PERMISSIONS = 1001

    override fun getViewModel() = vm

    private val surfaceCallBack = object : SurfaceHolder.Callback {

        override fun surfaceChanged(holder: SurfaceHolder?, format: Int, width: Int, height: Int) {
        }

        override fun surfaceDestroyed(holder: SurfaceHolder?) {
            cameraSource.main().stop()
        }

        @SuppressLint("MissingPermission")
        override fun surfaceCreated(holder: SurfaceHolder?) {

            if (allPermissionsGranted()) {
                cameraSource.main().start(holder)
            }
        }

    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentBarcodeScanBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if (allPermissionsGranted()) {
            setUpControls()
        } else {
            requestPermissions(REQUIRED_PERMISSIONS, REQUEST_CODE_PERMISSIONS)
        }
    }


    private fun setUpControls() {
        binding.cameraSurfaceView.holder.addCallback(surfaceCallBack)
    }


    private fun allPermissionsGranted(): Boolean {
        if (context != null) {
            for (permission in REQUIRED_PERMISSIONS) {
                if (ContextCompat.checkSelfPermission(
                        requireContext(),
                        permission
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    return false
                }
            }
            return true
        }
        return false
    }

    @SuppressLint("MissingPermission")
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_CODE_PERMISSIONS) {
            if (allPermissionsGranted()) {
                cameraSource.main().start(binding.cameraSurfaceView.holder)
            } else {
                //todo: manage it properly
                "Not granted".toast(context)
            }
        }
    }

    override fun setUpMainObservers() {
        super.setUpMainObservers()
        getViewModel().barcodeScanned.observe(Observer(this@BarcodeScanFragment::observedBarcode))

    }

    private fun observedBarcode(barcode: String) {
        binding.barcodeNumberInputEditText.setText(barcode)
    }

    // todo: all stuff about localbroadcast could be in abstract

    override fun onResume() {
        super.onResume()
        localBroadcastManager.registerReceiver(
            receiver,
            IntentFilter(DetectorProvider.INTENT_CUSTOM_SCAN)
        )
    }

    override fun onPause() {
        super.onPause()
        localBroadcastManager.unregisterReceiver(receiver)
    }

    override fun onDestroy() {
        super.onDestroy()
        detector.main().release()
        cameraSource.main().stop()
        cameraSource.main().release()
        localBroadcastManager.unregisterReceiver(receiver)
    }

    override fun setUpListeners() {
        binding.searchFoodButton.setOnClickListener(this@BarcodeScanFragment)
    }

    override fun onClick(view: View) {
        getViewModel().onClick(view.id)
    }

}


