package com.example.cb.presentation.features.barcode_scan

import android.content.Context
import com.google.android.gms.vision.CameraSource

class DefaultCameraProvider(val context: Context, val detectorProvider: DetectorProvider) :
    CameraProvider {
    override fun main(): CameraSource {
        return CameraSource.Builder(context, detectorProvider.main())
            .setRequestedFps(25f)
            .setAutoFocusEnabled(true).build()
    }


}