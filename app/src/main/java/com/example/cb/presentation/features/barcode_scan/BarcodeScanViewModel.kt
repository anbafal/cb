package com.example.cb.presentation.features.barcode_scan

import androidx.annotation.IdRes
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import com.example.cb.R
import com.example.cb.domain.Food
import com.example.cb.presentation.base.BaseViewModel

class BarcodeScanViewModel @ViewModelInject constructor(
    @Assisted savedStateHandle: SavedStateHandle
) :
    BaseViewModel(savedStateHandle) {

    private val _barcodeScanned: MutableLiveData<String> by lazy {
        MutableLiveData<String>()
    }
    internal val barcodeScanned: LiveData<String>
        get() = _barcodeScanned


    private val _foodReceived: MutableLiveData<Food> by lazy {
        MutableLiveData<Food>()
    }
    internal val foodReceived: LiveData<Food>
        get() = _foodReceived


    fun onScanResult(result: String) {
        val oldBarcodeScanned = _barcodeScanned.value ?: ""
        if (oldBarcodeScanned != result) {
            _barcodeScanned.postValue(result)
        }
    }

    fun onClick(@IdRes viewId: Int) {
        when (viewId) {
            R.id.search_food_button -> launchSearch()
        }
    }

    private fun launchSearch() {
        _barcodeScanned.value?.let {
            navigate(BarcodeScanFragmentDirections.actionBarcodeScanFragmentToSearchFoodFragment(it))
        }
    }


}
