package com.example.cb.presentation.features.splash

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import com.example.cb.presentation.base.BaseViewModel

class SplashViewModel @ViewModelInject constructor(
    @Assisted savedStateHandle: SavedStateHandle
) :
    BaseViewModel(savedStateHandle) {

    init {
        navigate(SplashFragmentDirections.actionSplashFragmentToFoodListFragment())
    }


    override fun onCleared() {
        super.onCleared()
    }
}
