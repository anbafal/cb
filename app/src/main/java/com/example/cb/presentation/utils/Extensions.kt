package com.example.cb.presentation.utils

import android.content.Context
import android.view.View
import android.widget.Toast
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer


interface ObserveUtils {
    fun <T> LiveData<T>.observe(observer: Observer<T>) {
        this.observe(getObserveLifecycleOwner(), observer)
    }

    fun getObserveLifecycleOwner(): LifecycleOwner
}

fun View.visibleWhen(show: Boolean) {
    if (show) this.visibility = View.VISIBLE
    else this.visibility = View.GONE
}


fun Any.toast(context: Context?, duration: Int = Toast.LENGTH_LONG): Toast? {
    return context?.let {
        Toast.makeText(it, this.toString(), duration).apply { show() }
    }

}