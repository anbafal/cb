package com.example.cb.presentation.utils

import androidx.annotation.IdRes
import androidx.navigation.NavDirections

sealed class NavigationCommand {
    data class To(val directions: NavDirections) : NavigationCommand()
    object Back : NavigationCommand()
    data class BackTo(@IdRes val destinationId: Int) : NavigationCommand()
    object ToRoot : NavigationCommand()
}