package com.example.cb.presentation.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.navigation.NavDirections
import com.example.cb.presentation.features.loader.IViewModelLoader
import com.example.cb.presentation.features.loader.LoaderUI
import com.example.cb.presentation.utils.NavigationCommand
import com.example.cb.presentation.utils.SingleLiveEvent


abstract class BaseViewModel(private val savedStateHandle: SavedStateHandle) : ViewModel(),
    IViewModelLoader {

    private val _navigationCommand: SingleLiveEvent<NavigationCommand> by lazy {
        SingleLiveEvent<NavigationCommand>()
    }
    internal val navigationCommand: LiveData<NavigationCommand>
        get() = _navigationCommand

    fun navigate(directions: NavDirections?) {
        if (directions != null) _navigationCommand.postValue(NavigationCommand.To(directions))
        else _navigationCommand.postValue(
            NavigationCommand.ToRoot
        )


    }

    val _showLoading: MutableLiveData<LoaderUI> by lazy {
        MutableLiveData<LoaderUI>()
    }
    override val showLoading: LiveData<LoaderUI>
        get() = _showLoading


    protected val _toastMessage: SingleLiveEvent<String> by lazy {
        SingleLiveEvent<String>()
    }
    val toastMessage: LiveData<String>
        get() = _toastMessage

}