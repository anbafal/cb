package com.example.cb.presentation.features.barcode_scan

import android.content.Context
import android.content.Intent
import androidx.core.util.isNotEmpty
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.example.cb.presentation.features.barcode_scan.DetectorProvider.Companion.INTENT_CUSTOM_SCAN
import com.example.cb.presentation.features.barcode_scan.DetectorProvider.Companion.INTENT_CUSTOM_SCAN_DATA
import com.google.android.gms.vision.Detector
import com.google.android.gms.vision.barcode.Barcode
import com.google.android.gms.vision.barcode.BarcodeDetector

class DefaultDetectorProvider(
    val context: Context,
    val localBroadcastManager: LocalBroadcastManager
) : DetectorProvider {

    private val processor = object : Detector.Processor<Barcode> {
        override fun release() {
        }

        override fun receiveDetections(detections: Detector.Detections<Barcode>?) {
            if (detections != null && detections.detectedItems.isNotEmpty()) {
                val barcode = detections?.detectedItems
                if (barcode?.size() ?: 0 > 0) {
                    Intent(INTENT_CUSTOM_SCAN).putExtra(
                        INTENT_CUSTOM_SCAN_DATA,
                        barcode?.valueAt(0)?.displayValue ?: ""
                    ).apply {
                        localBroadcastManager.sendBroadcast(this)
                    }
                }
            }
        }
    }

    override fun main(): BarcodeDetector {
        val detector = BarcodeDetector.Builder(context).setBarcodeFormats(Barcode.EAN_13).build()
        detector.setProcessor(processor)
        return detector
    }
}