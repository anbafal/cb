package com.example.cb.presentation.features.food.distant_search

import android.view.View
import androidx.annotation.IdRes
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import com.example.cb.R
import com.example.cb.ResourceProvider
import com.example.cb.domain.Food
import com.example.cb.domain.usecase.food.add.AddFoodUseCase
import com.example.cb.domain.usecase.food.search.SearchFoodUseCase
import com.example.cb.domain.utils.Utils
import com.example.cb.presentation.base.BaseViewModel
import com.example.cb.presentation.features.loader.LoaderUI
import com.example.cb.presentation.utils.SingleLiveEvent

class SearchFoodViewModel @ViewModelInject constructor(
    @Assisted savedStateHandle: SavedStateHandle,
    private val searchFoodUseCase: SearchFoodUseCase,
    private val addFoodUseCase: AddFoodUseCase,
    private val resourceProvider: ResourceProvider
) :
    BaseViewModel(savedStateHandle) {


    private lateinit var foodId: String

    private val _foodReceived: MutableLiveData<Food> by lazy {
        MutableLiveData<Food>()
    }
    internal val foodReceived: LiveData<Food>
        get() = _foodReceived

    private val _openCalendar: SingleLiveEvent<Boolean> by lazy {
        SingleLiveEvent<Boolean>()
    }
    val openCalendar: LiveData<Boolean>
        get() = _openCalendar


    fun onLaunch(foodId: String) {
        //this.foodId = foodId
        _showLoading.postValue(LoaderUI(View.VISIBLE, LoaderUI.SPLASH_SCREEN_DEFAULT_OPACITY))
        searchFoodUseCase.execute(foodId) {
            onComplete { food ->
                _showLoading.postValue(LoaderUI(View.GONE))
                _foodReceived.postValue(food)
            }
            onError { error ->
                _showLoading.postValue(LoaderUI(View.GONE))
                _toastMessage.postValue(error)
            }
            onCancel {
                _showLoading.postValue(LoaderUI(View.GONE))
            }
        }
    }


    fun onClick(@IdRes viewId: Int) {
        when (viewId) {
            R.id.add_product_button -> addProduct()
            R.id.expire_date_input_edit_text -> _openCalendar.value = true
        }
    }

    fun onSelectExpiredDate(expiredDate: Long) {
        _openCalendar.value = false
        _foodReceived.postValue(
            _foodReceived.value?.copy(
                expired = Utils.longDateToString(
                    expiredDate
                )
            )
        )
    }

    private fun addProduct() {
        _foodReceived.value?.let {
            _showLoading.postValue(LoaderUI(View.VISIBLE, LoaderUI.SPLASH_SCREEN_DEFAULT_OPACITY))
            addFoodUseCase.execute(it) {
                onComplete {
                    _showLoading.postValue(LoaderUI(View.GONE))
                    _toastMessage.postValue(resourceProvider.getString(R.string.add_product_message))
                    navigate(null)

                }
                onError { error ->
                    _showLoading.postValue(LoaderUI(View.GONE))
                    _toastMessage.postValue(error)
                }
                onCancel {
                    _showLoading.postValue(LoaderUI(View.GONE))
                }
            }
        }

    }


    override fun onCleared() {
        super.onCleared()
        searchFoodUseCase.unsubscribe()
        addFoodUseCase.unsubscribe()
    }


}
