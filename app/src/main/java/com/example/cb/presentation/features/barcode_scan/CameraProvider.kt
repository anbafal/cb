package com.example.cb.presentation.features.barcode_scan

import com.google.android.gms.vision.CameraSource

interface CameraProvider {
    fun main(): CameraSource
}