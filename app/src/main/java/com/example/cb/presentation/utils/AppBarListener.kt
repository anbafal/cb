package com.example.cb.presentation.utils

interface AppBarListener {
    fun displayAppBar(shouldDisplay: Boolean)
}