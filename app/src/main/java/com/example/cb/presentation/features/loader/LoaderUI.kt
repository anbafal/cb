package com.example.cb.presentation.features.loader

import android.view.View
import android.view.WindowManager

data class LoaderUI(
    val visibilityProgressBar: Int,
    val opacity: Float = 0.3F,
    val message: String = ""
) {

    fun shouldDisabledUI(): Boolean {
        return visibilityProgressBar == View.VISIBLE
    }

    val alphaScreen: Float
        get() = if (visibilityProgressBar == View.VISIBLE) {
            opacity
        } else {
            1F
        }

    companion object {
        const val FLAG_ACTIVITY_DISABLED = WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
        const val SPLASH_SCREEN_DEFAULT_OPACITY = 0.7F
    }

}