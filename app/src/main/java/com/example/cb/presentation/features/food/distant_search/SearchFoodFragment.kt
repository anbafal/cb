package com.example.cb.presentation.features.food.distant_search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import com.example.cb.databinding.FragmentSearchFoodBinding
import com.example.cb.domain.Food
import com.example.cb.presentation.base.BaseFragment
import com.google.android.material.datepicker.MaterialDatePicker
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SearchFoodFragment : BaseFragment<FragmentSearchFoodBinding, SearchFoodViewModel>(),
    View.OnClickListener {

    private val vm: SearchFoodViewModel by viewModels()
    private val navigationArgs: SearchFoodFragmentArgs by navArgs()

    override fun getViewModel() = vm

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentSearchFoodBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        getViewModel().onLaunch(navigationArgs.foodId)
    }

    override fun setUpMainObservers() {
        super.setUpMainObservers()
        with(getViewModel()) {
            foodReceived.observe(Observer(this@SearchFoodFragment::observedFood))
            openCalendar.observe(Observer(this@SearchFoodFragment::observedCalendar))
        }
    }

    private fun observedFood(food: Food) {
        with(binding) {
            searchBarcodeNumberInputEditText.setText(food.id)
            searchBarcodeNameInputEditText.setText(food.name)
            expireDateInputEditText.setText(food.expired)
        }
    }

    private fun observedCalendar(open: Boolean) {
        if (open) {
            activity?.supportFragmentManager?.let {
                //todo: DI
                val builder = MaterialDatePicker.Builder.datePicker()
                val picker = builder.build()
                picker.apply {
                    show(it, this.toString())
                    addOnPositiveButtonClickListener {
                        getViewModel().onSelectExpiredDate(it)
                    }
                }
            }
        }
    }


    override fun setUpListeners() {
        with(binding) {
            addProductButton.setOnClickListener(this@SearchFoodFragment)
            expireDateInputEditText.setOnClickListener(this@SearchFoodFragment)
        }

    }

    override fun onClick(view: View) {
        getViewModel().onClick(view.id)
    }


}