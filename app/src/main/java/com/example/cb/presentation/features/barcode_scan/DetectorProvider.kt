package com.example.cb.presentation.features.barcode_scan

import com.google.android.gms.vision.barcode.BarcodeDetector

interface DetectorProvider {
    fun main(): BarcodeDetector

    companion object {
        val INTENT_CUSTOM_SCAN = "INTENT_CUSTOM_SCAN"
        val INTENT_CUSTOM_SCAN_DATA = "INTENT_CUSTOM_SCAN_DATA"
    }
}