package com.example.cb.presentation.features.loader

interface IFragmentLoaderListener {
    fun showLoader(loaderUI: LoaderUI)
}