package com.example.cb.presentation.features.food.listing

import androidx.annotation.IdRes
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import com.example.cb.R
import com.example.cb.presentation.base.BaseViewModel

class FoodListViewModel @ViewModelInject constructor(
    @Assisted savedStateHandle: SavedStateHandle
) :
    BaseViewModel(savedStateHandle) {

    fun onClick(@IdRes viewId: Int) {
        when (viewId) {
            R.id.scan_button -> navigate(FoodListFragmentDirections.actionFoodListFragmentToBarcodeScanFragment())
        }
    }


}
