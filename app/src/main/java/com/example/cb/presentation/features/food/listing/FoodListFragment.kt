package com.example.cb.presentation.features.food.listing

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.example.cb.databinding.FragmentFoodListBinding
import com.example.cb.presentation.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FoodListFragment : BaseFragment<FragmentFoodListBinding, FoodListViewModel>(),
    View.OnClickListener {

    private val vm: FoodListViewModel by viewModels()

    override fun getViewModel() = vm

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentFoodListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onClick(view: View) {
        getViewModel().onClick(view.id)
    }

    override fun setUpListeners() {
        binding.scanButton.setOnClickListener(this@FoodListFragment)
    }


}