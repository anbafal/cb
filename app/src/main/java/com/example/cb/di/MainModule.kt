package com.example.cb.di

import android.content.Context
import android.content.SharedPreferences
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.example.cb.ResourceProvider
import com.example.cb.domain.utils.DefaultDispatcherProvider
import com.example.cb.domain.utils.DispatcherProvider
import com.example.cb.presentation.features.barcode_scan.CameraProvider
import com.example.cb.presentation.features.barcode_scan.DefaultCameraProvider
import com.example.cb.presentation.features.barcode_scan.DefaultDetectorProvider
import com.example.cb.presentation.features.barcode_scan.DetectorProvider
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object MainModule {

    @Provides
    @Singleton
    fun provideSharedPreferences(@ApplicationContext context: Context): SharedPreferences {
        val PREF_NAME = "SHARED_PREF"
        return context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
    }

    @Provides
    @Singleton
    fun providesDispatcher(): DispatcherProvider {
        return DefaultDispatcherProvider()
    }

    @Provides
    @Singleton
    fun providesDetector(
        @ApplicationContext context: Context,
        localBroadcastManager: LocalBroadcastManager
    ): DetectorProvider {
        return DefaultDetectorProvider(context, localBroadcastManager)
    }

    @Provides
    @Singleton
    fun providesCameraSource(
        @ApplicationContext context: Context,
        detectorProvider: DetectorProvider
    ): CameraProvider {
        return DefaultCameraProvider(context, detectorProvider)
    }

    @Provides
    @Singleton
    fun providesLocalBroadcast(@ApplicationContext context: Context): LocalBroadcastManager {
        return LocalBroadcastManager.getInstance(context)
    }

    @Provides
    @Singleton
    fun provideResourceProvider(@ApplicationContext context: Context): ResourceProvider {
        return ResourceProvider(context)
    }
}