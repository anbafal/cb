package com.example.cb.di

import android.content.Context
import androidx.room.Room
import com.example.cb.data.repository.FoodRepository
import com.example.cb.domain.repository.IFoodRepository
import com.example.cb.framework.retrofit.OpenFoodService
import com.example.cb.framework.room.AppDatabase
import com.example.cb.framework.room.RoomConstants
import com.example.cb.framework.room.dao.FoodDao

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object RoomModule {


    @Provides
    @Singleton
    fun providesRoomDatabase(@ApplicationContext context: Context): AppDatabase {
        return Room.databaseBuilder(context, AppDatabase::class.java, RoomConstants.DATABASE_NAME)
            .build()
    }

    @Provides
    @Singleton
    fun providesFoodDao(database: AppDatabase): FoodDao {
        return database.foodDao()
    }

    @Provides
    @Singleton
    fun providesFoodRepository(
        openFoodService: OpenFoodService,
        foodDao: FoodDao
    ): IFoodRepository {
        return FoodRepository(openFoodService, foodDao)
    }

}