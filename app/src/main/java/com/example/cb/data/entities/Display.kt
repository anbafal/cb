package com.example.cb.data.entities


import com.google.gson.annotations.SerializedName

data class Display(
    @SerializedName("en")
    val en: String
)