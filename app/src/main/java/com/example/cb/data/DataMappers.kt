package com.example.cb.data

import com.example.cb.data.entities.FoodDto
import com.example.cb.domain.Food


fun FoodDto.toDomain() =
    Food(id = code, name = product.productName, expired = "", picture = product.imageFrontUrl)