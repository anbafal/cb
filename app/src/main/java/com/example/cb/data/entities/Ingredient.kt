package com.example.cb.data.entities


import com.google.gson.annotations.SerializedName

data class Ingredient(
    @SerializedName("has_sub_ingredients")
    val hasSubIngredients: String,
    @SerializedName("id")
    val id: String,
    @SerializedName("percent_max")
    val percentMax: String,
    @SerializedName("percent_min")
    val percentMin: String,
    @SerializedName("processing")
    val processing: String,
    @SerializedName("rank")
    val rank: String,
    @SerializedName("text")
    val text: String,
    @SerializedName("vegan")
    val vegan: String,
    @SerializedName("vegetarian")
    val vegetarian: String
)