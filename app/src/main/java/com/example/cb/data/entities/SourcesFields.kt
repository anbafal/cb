package com.example.cb.data.entities


import com.google.gson.annotations.SerializedName

data class SourcesFields(
    @SerializedName("org-database-usda")
    val orgDatabaseUsda: OrgDatabaseUsda
)