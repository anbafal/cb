package com.example.cb.data.entities


import com.google.gson.annotations.SerializedName

data class X400XXXXXXXX(
    @SerializedName("h")
    val h: String,
    @SerializedName("w")
    val w: String
)