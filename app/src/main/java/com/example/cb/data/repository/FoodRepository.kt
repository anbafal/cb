package com.example.cb.data.repository

import com.example.cb.data.toDomain
import com.example.cb.domain.Food
import com.example.cb.domain.repository.IFoodRepository
import com.example.cb.framework.retrofit.OpenFoodService
import com.example.cb.framework.room.dao.FoodDao
import com.example.cb.framework.toBean

class FoodRepository(private val foodService: OpenFoodService, private val foodDao: FoodDao) :
    IFoodRepository {
    override suspend fun searchFoodWithId(foodId: String): Food {
        val foodDto = foodService.searchFoodAsync(foodId).await()
        return foodDto.toDomain()
    }

    override suspend fun addFood(food: Food): Long {
        val foodBean = food.toBean()
        return foodDao.insert(foodBean)
    }

}