package com.example.cb.data.entities


import com.google.gson.annotations.SerializedName

data class SizesXXXXXXXX(
    @SerializedName("full")
    val full: FullXXXXXXXX,
    @SerializedName("100")
    val x100: X100XXXXXXXX,
    @SerializedName("400")
    val x400: X400XXXXXXXX
)