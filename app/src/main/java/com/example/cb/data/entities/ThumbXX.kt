package com.example.cb.data.entities


import com.google.gson.annotations.SerializedName

data class ThumbXX(
    @SerializedName("en")
    val en: String
)