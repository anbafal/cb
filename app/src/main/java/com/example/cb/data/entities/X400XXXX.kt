package com.example.cb.data.entities


import com.google.gson.annotations.SerializedName

data class X400XXXX(
    @SerializedName("h")
    val h: String,
    @SerializedName("w")
    val w: String
)