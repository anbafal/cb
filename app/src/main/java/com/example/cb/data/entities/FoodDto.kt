package com.example.cb.data.entities


import com.google.gson.annotations.SerializedName

data class FoodDto(
    @SerializedName("code")
    val code: String,
    @SerializedName("product")
    val product: Product,
    @SerializedName("status")
    val status: String,
    @SerializedName("status_verbose")
    val statusVerbose: String
)