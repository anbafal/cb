package com.example.cb.data.entities


import com.google.gson.annotations.SerializedName

data class X2(
    @SerializedName("sizes")
    val sizes: SizesXXXXXXX,
    @SerializedName("uploaded_t")
    val uploadedT: String,
    @SerializedName("uploader")
    val uploader: String
)