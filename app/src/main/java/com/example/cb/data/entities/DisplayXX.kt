package com.example.cb.data.entities


import com.google.gson.annotations.SerializedName

data class DisplayXX(
    @SerializedName("en")
    val en: String
)