package com.example.cb.data.entities


import com.google.gson.annotations.SerializedName

data class SizesXXXXXXXXXX(
    @SerializedName("full")
    val full: FullXXXXXXXXXX,
    @SerializedName("100")
    val x100: X100XXXXXXXXXX,
    @SerializedName("400")
    val x400: X400XXXXXXXXXX
)