package com.example.cb.data.entities


import com.google.gson.annotations.SerializedName

data class X4(
    @SerializedName("sizes")
    val sizes: SizesXXXXXXXXX,
    @SerializedName("uploaded_t")
    val uploadedT: String,
    @SerializedName("uploader")
    val uploader: String
)