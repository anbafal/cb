package com.example.cb.data.entities


import com.google.gson.annotations.SerializedName

data class DisplayX(
    @SerializedName("en")
    val en: String
)