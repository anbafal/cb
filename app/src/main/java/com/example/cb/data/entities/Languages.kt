package com.example.cb.data.entities


import com.google.gson.annotations.SerializedName

data class Languages(
    @SerializedName("en:english")
    val enEnglish: String
)