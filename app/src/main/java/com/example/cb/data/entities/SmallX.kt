package com.example.cb.data.entities


import com.google.gson.annotations.SerializedName

data class SmallX(
    @SerializedName("en")
    val en: String
)