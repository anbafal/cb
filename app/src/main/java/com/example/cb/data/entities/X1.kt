package com.example.cb.data.entities


import com.google.gson.annotations.SerializedName

data class X1(
    @SerializedName("sizes")
    val sizes: SizesXXXXXX,
    @SerializedName("uploaded_t")
    val uploadedT: String,
    @SerializedName("uploader")
    val uploader: String
)