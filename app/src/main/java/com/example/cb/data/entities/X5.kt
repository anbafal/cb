package com.example.cb.data.entities


import com.google.gson.annotations.SerializedName

data class X5(
    @SerializedName("sizes")
    val sizes: SizesXXXXXXXXXX,
    @SerializedName("uploaded_t")
    val uploadedT: String,
    @SerializedName("uploader")
    val uploader: String
)