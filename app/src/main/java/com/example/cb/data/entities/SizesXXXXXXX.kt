package com.example.cb.data.entities


import com.google.gson.annotations.SerializedName

data class SizesXXXXXXX(
    @SerializedName("full")
    val full: FullXXXXXXX,
    @SerializedName("100")
    val x100: X100XXXXXXX,
    @SerializedName("400")
    val x400: X400XXXXXXX
)