package com.example.cb.data.entities


import com.google.gson.annotations.SerializedName

data class Nutriments(
    @SerializedName("calcium")
    val calcium: String,
    @SerializedName("calcium_100g")
    val calcium100g: String,
    @SerializedName("calcium_serving")
    val calciumServing: String,
    @SerializedName("calcium_unit")
    val calciumUnit: String,
    @SerializedName("calcium_value")
    val calciumValue: String,
    @SerializedName("carbohydrates")
    val carbohydrates: String,
    @SerializedName("carbohydrates_100g")
    val carbohydrates100g: String,
    @SerializedName("carbohydrates_serving")
    val carbohydratesServing: String,
    @SerializedName("carbohydrates_unit")
    val carbohydratesUnit: String,
    @SerializedName("carbohydrates_value")
    val carbohydratesValue: String,
    @SerializedName("cholesterol")
    val cholesterol: String,
    @SerializedName("cholesterol_100g")
    val cholesterol100g: String,
    @SerializedName("cholesterol_serving")
    val cholesterolServing: String,
    @SerializedName("cholesterol_unit")
    val cholesterolUnit: String,
    @SerializedName("cholesterol_value")
    val cholesterolValue: String,
    @SerializedName("energy")
    val energy: String,
    @SerializedName("energy_100g")
    val energy100g: String,
    @SerializedName("energy-kcal")
    val energyKcal: String,
    @SerializedName("energy-kcal_100g")
    val energyKcal100g: String,
    @SerializedName("energy-kcal_serving")
    val energyKcalServing: String,
    @SerializedName("energy-kcal_unit")
    val energyKcalUnit: String,
    @SerializedName("energy-kcal_value")
    val energyKcalValue: String,
    @SerializedName("energy_serving")
    val energyServing: String,
    @SerializedName("energy_unit")
    val energyUnit: String,
    @SerializedName("energy_value")
    val energyValue: String,
    @SerializedName("fat")
    val fat: String,
    @SerializedName("fat_100g")
    val fat100g: String,
    @SerializedName("fat_serving")
    val fatServing: String,
    @SerializedName("fat_unit")
    val fatUnit: String,
    @SerializedName("fat_value")
    val fatValue: String,
    @SerializedName("fiber")
    val fiber: String,
    @SerializedName("fiber_100g")
    val fiber100g: String,
    @SerializedName("fiber_serving")
    val fiberServing: String,
    @SerializedName("fiber_unit")
    val fiberUnit: String,
    @SerializedName("fiber_value")
    val fiberValue: String,
    @SerializedName("fruits-vegetables-nuts-estimate-from-ingredients_100g")
    val fruitsVegetablesNutsEstimateFromIngredients100g: String,
    @SerializedName("iron")
    val iron: String,
    @SerializedName("iron_100g")
    val iron100g: String,
    @SerializedName("iron_serving")
    val ironServing: String,
    @SerializedName("iron_unit")
    val ironUnit: String,
    @SerializedName("iron_value")
    val ironValue: String,
    @SerializedName("nova-group")
    val novaGroup: String,
    @SerializedName("nova-group_100g")
    val novaGroup100g: String,
    @SerializedName("nova-group_serving")
    val novaGroupServing: String,
    @SerializedName("nutrition-score-fr")
    val nutritionScoreFr: String,
    @SerializedName("nutrition-score-fr_100g")
    val nutritionScoreFr100g: String,
    @SerializedName("nutrition-score-fr_serving")
    val nutritionScoreFrServing: String,
    @SerializedName("proteins")
    val proteins: String,
    @SerializedName("proteins_100g")
    val proteins100g: String,
    @SerializedName("proteins_serving")
    val proteinsServing: String,
    @SerializedName("proteins_unit")
    val proteinsUnit: String,
    @SerializedName("proteins_value")
    val proteinsValue: String,
    @SerializedName("salt")
    val salt: String,
    @SerializedName("salt_100g")
    val salt100g: String,
    @SerializedName("salt_serving")
    val saltServing: String,
    @SerializedName("salt_unit")
    val saltUnit: String,
    @SerializedName("salt_value")
    val saltValue: String,
    @SerializedName("saturated-fat")
    val saturatedFat: String,
    @SerializedName("saturated-fat_100g")
    val saturatedFat100g: String,
    @SerializedName("saturated-fat_serving")
    val saturatedFatServing: String,
    @SerializedName("saturated-fat_unit")
    val saturatedFatUnit: String,
    @SerializedName("saturated-fat_value")
    val saturatedFatValue: String,
    @SerializedName("sodium")
    val sodium: String,
    @SerializedName("sodium_100g")
    val sodium100g: String,
    @SerializedName("sodium_serving")
    val sodiumServing: String,
    @SerializedName("sodium_unit")
    val sodiumUnit: String,
    @SerializedName("sodium_value")
    val sodiumValue: String,
    @SerializedName("sugars")
    val sugars: String,
    @SerializedName("sugars_100g")
    val sugars100g: String,
    @SerializedName("sugars_serving")
    val sugarsServing: String,
    @SerializedName("sugars_unit")
    val sugarsUnit: String,
    @SerializedName("sugars_value")
    val sugarsValue: String,
    @SerializedName("trans-fat")
    val transFat: String,
    @SerializedName("trans-fat_100g")
    val transFat100g: String,
    @SerializedName("trans-fat_serving")
    val transFatServing: String,
    @SerializedName("trans-fat_unit")
    val transFatUnit: String,
    @SerializedName("trans-fat_value")
    val transFatValue: String,
    @SerializedName("vitamin-a")
    val vitaminA: String,
    @SerializedName("vitamin-a_100g")
    val vitaminA100g: String,
    @SerializedName("vitamin-a_serving")
    val vitaminAServing: String,
    @SerializedName("vitamin-a_unit")
    val vitaminAUnit: String,
    @SerializedName("vitamin-a_value")
    val vitaminAValue: String,
    @SerializedName("vitamin-c")
    val vitaminC: String,
    @SerializedName("vitamin-c_100g")
    val vitaminC100g: String,
    @SerializedName("vitamin-c_serving")
    val vitaminCServing: String,
    @SerializedName("vitamin-c_unit")
    val vitaminCUnit: String,
    @SerializedName("vitamin-c_value")
    val vitaminCValue: String
)