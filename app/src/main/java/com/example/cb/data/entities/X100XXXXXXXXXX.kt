package com.example.cb.data.entities


import com.google.gson.annotations.SerializedName

data class X100XXXXXXXXXX(
    @SerializedName("h")
    val h: String,
    @SerializedName("w")
    val w: String
)