package com.example.cb.data.entities


import com.google.gson.annotations.SerializedName

data class NutriscoreData(
    @SerializedName("energy")
    val energy: String,
    @SerializedName("energy_points")
    val energyPoints: String,
    @SerializedName("energy_value")
    val energyValue: String,
    @SerializedName("fiber")
    val fiber: String,
    @SerializedName("fiber_points")
    val fiberPoints: String,
    @SerializedName("fiber_value")
    val fiberValue: String,
    @SerializedName("fruits_vegetables_nuts_colza_walnut_olive_oils")
    val fruitsVegetablesNutsColzaWalnutOliveOils: String,
    @SerializedName("fruits_vegetables_nuts_colza_walnut_olive_oils_points")
    val fruitsVegetablesNutsColzaWalnutOliveOilsPoints: String,
    @SerializedName("fruits_vegetables_nuts_colza_walnut_olive_oils_value")
    val fruitsVegetablesNutsColzaWalnutOliveOilsValue: String,
    @SerializedName("grade")
    val grade: String,
    @SerializedName("is_beverage")
    val isBeverage: String,
    @SerializedName("is_cheese")
    val isCheese: String,
    @SerializedName("is_fat")
    val isFat: String,
    @SerializedName("is_water")
    val isWater: String,
    @SerializedName("negative_points")
    val negativePoints: String,
    @SerializedName("positive_points")
    val positivePoints: String,
    @SerializedName("proteins")
    val proteins: String,
    @SerializedName("proteins_points")
    val proteinsPoints: String,
    @SerializedName("proteins_value")
    val proteinsValue: String,
    @SerializedName("saturated_fat")
    val saturatedFat: String,
    @SerializedName("saturated_fat_points")
    val saturatedFatPoints: String,
    @SerializedName("saturated_fat_ratio")
    val saturatedFatRatio: String,
    @SerializedName("saturated_fat_ratio_points")
    val saturatedFatRatioPoints: String,
    @SerializedName("saturated_fat_ratio_value")
    val saturatedFatRatioValue: String,
    @SerializedName("saturated_fat_value")
    val saturatedFatValue: String,
    @SerializedName("score")
    val score: String,
    @SerializedName("sodium")
    val sodium: String,
    @SerializedName("sodium_points")
    val sodiumPoints: String,
    @SerializedName("sodium_value")
    val sodiumValue: String,
    @SerializedName("sugars")
    val sugars: String,
    @SerializedName("sugars_points")
    val sugarsPoints: String,
    @SerializedName("sugars_value")
    val sugarsValue: String
)