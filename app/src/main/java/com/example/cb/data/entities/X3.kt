package com.example.cb.data.entities


import com.google.gson.annotations.SerializedName

data class X3(
    @SerializedName("sizes")
    val sizes: SizesXXXXXXXX,
    @SerializedName("uploaded_t")
    val uploadedT: String,
    @SerializedName("uploader")
    val uploader: String
)