package com.example.cb.data.entities


import com.google.gson.annotations.SerializedName

data class LanguagesCodes(
    @SerializedName("en")
    val en: String
)