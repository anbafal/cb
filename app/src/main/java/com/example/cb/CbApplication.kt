package com.example.cb

import android.app.Application
import android.os.StrictMode
import com.facebook.stetho.Stetho
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class CbApplication : Application() {
    private var androidDefaultUEH: Thread.UncaughtExceptionHandler? = null

    // Catch exception when app crashes and restart
    private val handler = Thread.UncaughtExceptionHandler { thread, ex ->
        //LoggerManager.e(ex)
        androidDefaultUEH?.uncaughtException(thread, ex)
    }

    override fun onCreate() {
        super.onCreate()
        androidDefaultUEH = Thread.getDefaultUncaughtExceptionHandler()
        Thread.setDefaultUncaughtExceptionHandler(handler)

        if (BuildConfig.DEBUG) {
            StrictMode.setThreadPolicy(
                StrictMode.ThreadPolicy.Builder().detectDiskReads().detectDiskWrites()
                    .detectAll() // or .detectAll() for all detectable problems
                    .penaltyLog().build()
            )
            StrictMode.setVmPolicy(
                StrictMode.VmPolicy.Builder().detectLeakedSqlLiteObjects()
                    .detectLeakedClosableObjects().penaltyLog().penaltyDeath().build()
            )
            Stetho.initializeWithDefaults(this);
        }

    }
}