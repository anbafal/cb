package com.example.cb

import android.content.Context
import androidx.core.content.ContextCompat


class ResourceProvider(val context: Context) {

    fun getString(resId: Int) = context.getString(resId)

    fun getString(resId: Int, value: String) = context.getString(resId, value)

    fun getString(resId: Int, vararg args: Any): String? = context.getString(resId, *args)

    fun getDimension(resId: Int) = context.resources.getDimension(resId)

    fun getInteger(resId: Int) = context.resources.getInteger(resId)

    fun getDrawable(resId: Int) = ContextCompat.getDrawable(context, resId)

    fun getColor(resId: Int) = ContextCompat.getColor(context, resId)

}